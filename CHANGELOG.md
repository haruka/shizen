# Changelog

## 0.1.19 - yyyy/mm/dd

- Add Fediverse creator link

## 0.1.18 - 2024/09/14
- Fix new light theme colors (search)

## 0.1.17 - 2024/09/14
- New themes colors

## 0.1.16 - 2024/05/06

- Fix codeblock CSS
- Fix site title CSS text-decoration on hover

## 0.1.15 - 2024/04/03

- Homepage templating
- Add CSS support for social networks icons

## 0.1.14 - 2024/03/06

- Minors CSS fixes
- Remove the "blog" page

## 0.1.13 - 2024/03/02

- Handle home VS blog
- Refacto search form and navigation
- Minors CSS fixes

## 0.1.12 - 2024/01/19

- ADD color scheme switcher

## 0.1.11 - 2023/11/29

- FIX RSS tag link

## 0.1.10 - 2023/11/29

- ADD Luciole font-famiy #5
- ADD RSS for tags
- FIX rss.svg size

## 0.1.9 - 2023/11/02

- Add mastodon verification link

## 0.1.8 - 2023/10/27

- Homepage move date before the article title
- Fix home meta title if empty
- Fix search form accessibility
- Fix color contrast on light and dark theme
- Fix tags spacing for a best tap target on mobile device

## 0.1.7 - 2023/10/19

- Fix missing h1 on homepage
- Fix missing h1 on error page

## 0.1.6 - 2023/10/06

- Search light and dark background color
- Remove "tags" word on page
- Change font-family to sans-serif
- Add border-radius to code blocks

## 0.1.5 - 2023/09/30

- Fix code css style
- Fix homepage header line-height

## 0.1.4 - 2023/09/30

- Fix read duration missing space #17
- Center home.md content on homepage #19
- Fix tag inline display on homepage #20
- Fix missing pre/code CSS style #21
- Footer "Heiwa" mention changed #22

## 0.1.3 - 2023/09/20

- Applying CSS to the search input
- Remove breadcrumb from homepage

