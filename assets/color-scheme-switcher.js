const setColorScheme = (scheme) => {
  document.documentElement.className = scheme;
  localStorage.setItem('scheme', scheme);
}

const getScheme = () => {
  const scheme = localStorage.getItem('scheme');
  scheme && setColorScheme(scheme);
}

getScheme();
