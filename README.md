# Shizen
[![licence GPL](https://img.shields.io/badge/license-GPL_3.0-purple.svg)](https://codeberg.org/haruka/shizen/src/branch/main/LICENCE)
[![Mastodon](https://img.shields.io/badge/%40haruka-6768f3?logo=mastodon&logoColor=%23ffffff)](https://hachyderm.io/@haruka)
[![Ko-fi](https://img.shields.io/badge/Ko--fi-FF5E5B?logo=ko-fi&logoColor=white)](https://ko-fi.com/haruka7)

Shizen is the default theme for the Heiwa CMS

TODO screenshot

## Licence

This theme and the Heiwa project are under the GPLv3 licence : https://www.gnu.org/licenses/gpl-3.0.en.html

## Installation

TODO

## Contributing

TODO

## Support

For support, please create an issue on [Codeberg](https://codeberg.org/haruka/shizen/issues).

## Authors

Haruka, follow me on [Mastodon](https://hachyderm.io/@haruka) or support me on [Ko-fi](https://ko-fi.com/haruka7)
